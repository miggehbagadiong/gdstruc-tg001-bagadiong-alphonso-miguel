#include<iostream>
#include<string>
#include"UnorderedArray.h"
#include"Stack.h"
#include"Queue.h"

using namespace std;

void systemPC() {

	system("pause");
	system("cls");
}
int main() {

	cout << "input the size of the arrays: ";
	int arraySize;
	cin >> arraySize;

	Stack<int>stackedNumbers(arraySize);
	Queue<int>queuedNumbers(arraySize);

	while (true) {
		//input the rest of the code here
		cout << "what would you like to do?" << endl;
		cout << "1 - push elements" << endl;
		cout << "2 - pop elements" << endl;
		cout << "3 - show everything then empty set" << endl;
		int arrayChoice;
		cin >> arrayChoice;

		if (arrayChoice == 1) {
			//input function here
			cout << "input a number: ";
			int inputNumber;
			cin >> inputNumber;

			queuedNumbers.push(inputNumber);
			stackedNumbers.push(inputNumber);

			cout << "top elements of sets:" << endl;
			cout << "queue: " << queuedNumbers.top() << endl;
			cout << "stack: " << stackedNumbers.top() << endl;

			systemPC();
		}

		else if (arrayChoice == 2) {
			//issue: code stops after popping elements
			cout << "you have popped the front elements of the number" << endl;
			cout << endl;

			queuedNumbers.pop();
			stackedNumbers.pop();
			 
			//issue: not showing the top numbers suddenly after second loopng
			cout << "top elements of sets:" << endl;
			cout << "queue: " << queuedNumbers.top() << endl;
			cout << "stack: " << stackedNumbers.top() << endl;

			systemPC();
		}

		else if (arrayChoice == 3) {
			cout << "list of numbers:" << endl;

			cout << "queue:" << endl;
			for (int i = 0; i < queuedNumbers.getSize(); i++)
				cout << queuedNumbers[i] << endl;

			cout << "stack:" << endl;
			for (int i = 0; i < stackedNumbers.getSize(); i++)
				cout << stackedNumbers[i] << endl;

			queuedNumbers.remove();
			stackedNumbers.remove();
			
			systemPC();
		}
		
	}
	system("pause");
	return 0;
}