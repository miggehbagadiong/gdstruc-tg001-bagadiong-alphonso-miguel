#pragma once

#include "UnorderedArray.h"

//first in - last out
template <class T>

class Stack {

public:
	
	Stack(int size) : mNumbers(NULL) 
	{
		mNumbers = new UnorderedArray<T>(size);
	}

	virtual void push(T val)
	{
		mNumbers->push(val);
	}

	void pop()
	{
		mNumbers->pop();
	}

	virtual int getSize() 
	{
		return	mNumbers->getSize();
	}

	virtual T& operator[](int index)
	{
		return mNumbers[0][index];
	}

	virtual const T& top()
	{
		return mNumbers[0][mNumbers->getSize() - 1];
	}
	
	virtual void remove() 
	{
		for (int i = 0;i <= getSize() + 1; i++)
			mNumbers->pop();
	}

private:
	UnorderedArray<T>* mNumbers;
};