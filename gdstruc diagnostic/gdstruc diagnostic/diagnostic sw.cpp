#include<iostream>
#include<string>
#include<time.h>
#include<stdlib.h>

using namespace std;

int main()
{
	srand(time(NULL));
	int values[10];
	int i, j;

	cout << "random numbers:" << endl;
	for (i = 0; i < 10; i++)
	{
		values[i] = rand() % 100 + 1;
		cout << "value [" << i + 1 << "] : " << values[i] << endl;
	}

	cout << endl;
	cout << "how would you like to sort?" << endl;
	cout << "[1] ascending [2] descending" << endl;
	cout << "-> ";
	int sortChoice;
	cin >> sortChoice;

	if (sortChoice == 1)
	{
		cout << "sorting in ascending order" << endl;
		system("pause");

		int temp;

		cout << "values:" << endl;
		for (i = 0; i < 10; i++)
		{
			for (j = i; j < 10; j++)
			{
				if (values[i] > values[j])
				{
					temp = values[j];
					values[j] = values[i];
					values[i] = temp;
				}
			}
		}

		for (i = 0; i < 10; i++)
		{
			cout << "value [" << i + 1 << "] : " << values[i] << endl;
		}
	}
	else if (sortChoice == 2)
	{
		cout << "sorting in descending order" << endl;
		system("pause");

		int temp;

		cout << "values" << endl;
		for (i = 0; i < 10; i++)
		{
			for (j = i; j < 10; j++)
			{
				if (values[i] < values[j])
				{
					temp = values[j];
					values[j] = values[i];
					values[i] = temp;
				}
			}
		}

		for (i = 0; i < 10; i++)
		{
			cout << "value [" << i + 1 << "] : " << values[i] << endl;
		}
	}

	cout << endl;

	cout << "search for linear values. what number would you like to search?" << endl;
	cout << "-> ";
	int searchQ;
	cin >> searchQ;

	cout << endl;
	cout << "searching for value!!" << endl;
	system("pause");

	int searchCount = 1;
	for (i = 0; i < 10; i++)
	{
		if (searchQ == values[i])
		{
			cout << "value found" << endl;
			cout << "it took " << searchCount << " steps" << endl;
		}
		else if (searchQ != values[i])
		{
			continue;
			searchCount++;
		}
		else if (values[i] == 9)
		{
			cout << "value does not exist" << endl;
		}
	}

	system("pause");
	return 0;
}