#include<iostream>
#include<string>

using namespace std;

int recursiveSummation(int number) 
{
	// working but needs adjustments
	cout << number;
	if (number <= 0)
	{
		cout << "=";
		return 0;
	}
	else
	{
		cout << "+";
		return (number % 10 + recursiveSummation(number / 10));
	}
}

int recursiveFibonacci(int number)
{
	if (number == 1 || number == 0)
		return number;
	else
		return(recursiveFibonacci(number - 1) + recursiveFibonacci(number - 2));
}

void recursiveFibonacciSequence(int number)
{
	int i = 0;

	while (i < number)
	{
		cout << recursiveFibonacci(i) << " ";
		i++;
	}
}

bool isValuePrime(int number, int val)
{
	//base case
	if (number <= 2)
		return (number == 2) ? true : false;
	if (number % val == 0)
		return false;
	if (val * val > number)
		return true;
}

void recursivePrimeNumCheck(int number)
{
	int value = 2;

	isValuePrime(number, value);

	if (isValuePrime(number, value))
		cout << number << " is a prime value" << endl;
	else
		cout << number << " is not a prime value" << endl;
}

int main()
{
	int summationNum = 0, fibonnaciNum = 0, primeNum = 0;
	int summationResult = 0;

	cout << "input digits (sum of digits) : ";
	cin >> summationNum;
	
	cout << endl;
	cout << recursiveSummation(summationNum);
	
	cout << endl << endl;
	cout << "input size (fibonacci) : ";
	cin >> fibonnaciNum;

	cout << endl; 
	recursiveFibonacciSequence(fibonnaciNum);

	cout << endl << endl;
	cout << "input to check (prime number) : ";
	cin >> primeNum;
	cout << endl;
	recursivePrimeNumCheck(primeNum);
	
	cout << endl;
	system("pause");
	return 0;
}