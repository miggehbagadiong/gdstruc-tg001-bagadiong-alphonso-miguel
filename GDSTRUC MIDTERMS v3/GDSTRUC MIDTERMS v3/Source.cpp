#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

int main()
{
	cout << "Enter size of array: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	do 
	{
		cout << "\nGenerated array: " << endl;
		cout << "Unordered: ";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << "   ";
		cout << "\nOrdered: ";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << "   ";


		cout << endl;
		cout << "What would you like to do?" << endl;
		cout << "[1] Remove an element" << endl;
		cout << "[2] Search an element" << endl;
		cout << "[3] Expand and Auto-assign values" << endl;
		cout << "[4] EXIT" << endl;
		cout << "-> ";
		int userChoice;
		cin >> userChoice;

		if (userChoice == 1)
		{
			cout << "Select the INDEX that you would like to remove" << endl;
			int removeIndex;
			cin >> removeIndex;
			
			cout << "Number Removed" << endl;
			unordered.remove(removeIndex);
			ordered.remove(removeIndex);
		}
		else if (userChoice == 2)
		{
			cout << "\n\nEnter number to search: ";
			int input;
			cin >> input;
			cout << endl;

			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;

			cout << "Ordered Array(Binary Search):\n";
			result = ordered.binarySearch(input);

			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;

		}
		else if (userChoice == 3)
		{
			cout << "Expanding and auto assigning element" << endl;
			cout << "How many numbers would you like to push?" << endl;
			cout << "-> ";
			int addSize;
			cin >> addSize;

			for (int i = 0; i < addSize; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}
		}
		else if (userChoice == 4)
		{
			cout << "EXITING" << endl;
			break;
		}
	} while(true);
	
	system("pause");
	return 0;
}