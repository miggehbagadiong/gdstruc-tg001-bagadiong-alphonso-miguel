#include<iostream>
#include<string>
#include<time.h>
#include"UnorderedArray.h"

using namespace std;

int main()
{
	srand(time(NULL));
	cout << "enter the number of integers that you like" << endl;
	cout << "-> ";
	int numSize;
	cin >> numSize;

	UnorderedArray<int>values(numSize);
	for (int i = 0; i < numSize; i++)
	{
		int randomNum = rand() % 100 + 1;
		values.push(randomNum);
	}

	cout << "NUMBERS:" << endl;
	for (int i = 0; i < numSize; i++)
	{
		cout << values[i] << endl;
	}

	cout << "select an element that you would like to remove:" << endl;
	cout << "-> ";
	int selectElement;
	cin >> selectElement;

	values.remove(selectElement);
	values.pop();

	cout << "NUMBERS:" << endl;
	for (int i = 0; i < numSize; i++)
	{
		cout << values[i] << endl;
	}

	cout << "what would you like to search?" << endl;
	cout << "-> ";
	int searchNumber;
	cin >> searchNumber;

	for (int i = 0; i < numSize; i++)
	{
		if (searchNumber == values[i])
		{
			cout << values[i] << " found" << endl;
		}
		else
		{
			cout << "value not found" << endl;
		}

	}

	system("pause");
	return 0;
}